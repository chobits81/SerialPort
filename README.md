#SerialPort
该项目主要演示使用windows7模拟串口发送数据，使用Java读取串口数据<br/>
PS：本机环境 windows7 32位系统，本次所有使用软件在项目中soft目录下<br/>
<br/>

**一、**串口模拟软件使用VSPD 6，安装步骤如下：<br/>
1. 解压soft目录下的vspd_win_32.rar文件<br/>
2. 请先运行目录 NT6 下的vsbsetup.exe；<br/>
3. 返回根目录，直接运行vspdconfig.exe即可。<br/>
PS：以下仅以win7 32位进行安装测试，其它版本请在根目录选择相应NT版本，如win7 64位，则选NT6x64；X P 32位选NT5。<br/>
4. 运行好后，添加两个串口，PS：串口是一对一对添加的<br/>
5. 添加好后就能在软件的侧边栏的Physical ports下看到当前添加的两个串口</br>
<br/>

**二、**安装java读取串口的环境，安装步骤如下：<br/>
1. 将soft目录下的javaxcomm目录下的三个文件复制到JAVA_HOME下对应的位置<br/>
2. comm.jar放到JAVA_HOME/jre/lib/ext目录下<br/>
3. javax.comm.properties放到JAVA_HOME/jre/lib目录下<br/>
4. win32com.dll放到JAVA_HOME/jre/bin目录下<br/>
<br/>

**三、**数据的发送与接收测试：<br/>
1. 打开soft/util目录下的JavaRs232Case.exe软件，软件会自动检测当前所有的串口<br/>
2. 选择一个串口 点击 "打开"<br/>
3. 打开soft/util/串口小助手目录下的SSCOM32.EXE软件，选择前面添加的另一个串口打开，准备接收数据<br/>
4. JavaRs232Case.exe软件中填写要发送的内容，点击发送<br/>
5. 观察SSCOM32.EXE软件的接收输出的内容，如果一致则串口的发送接收配置没有问题，接下来就可以进行java读取串口的开发了<br/>
<br/>
<br/>
<br/>
配置eclipse开发环境，配置步骤如下：<br/>
1. 将comm.jar导入到项目Library中<br/>
2. 运行com.hxh.sp.SP中的入口方法，控制台则会输出当前从串口获取到的数据信息。<br/>
3. JavaRs232Case.exe软件中填写要发送的内容，点击发送，在控制台中就能接收到JavaRs232Case发送的信息<br/>
PS：该项目采取的为一直不停的读取串口数据，具体情况请根据项目实际需求修改